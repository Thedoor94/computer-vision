//OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//std:
#include <fstream>
#include <iostream>
#include <string>

//math
#include <cmath>

struct ArgumentList {
	std::string image_name;		    //!< image file name
	int wait_t;                     //!< waiting time
};

bool ParseInputs(ArgumentList& args, int argc, char **argv) {

	if(argc<3 || (argc==2 && std::string(argv[1]) == "--help") || (argc==2 && std::string(argv[1]) == "-h") || (argc==2 && std::string(argv[1]) == "-help"))
	{
		std::cout<<"usage: simple -i <image_name>"<<std::endl;
		std::cout<<"exit:  type q"<<std::endl<<std::endl;
		std::cout<<"Allowed options:"<<std::endl<<
				"   -h	                     produce help message"<<std::endl<<
				"   -i arg                   image name. Use %0xd format for multiple images."<<std::endl<<
				"   -t arg                   wait before next frame (ms) [default = 0]"<<std::endl<<std::endl<<std::endl;
		return false;
	}

	int i = 1;
	while(i<argc)
	{
		if(std::string(argv[i]) == "-i") {
			args.image_name = std::string(argv[++i]);
		}

		if(std::string(argv[i]) == "-t") {
			args.wait_t = atoi(argv[++i]);
		}
		else
			args.wait_t = 0;

		++i;
	}

	return true;
}
	
	
	void grey_float (const cv:: Mat& M, cv:: Mat& output_conv) {
		
		output_conv = cv::Mat (M.rows, M.cols, CV_32FC1);
		
		float* convert_float = reinterpret_cast <float *> (output_conv.data);	
						
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				convert_float[u+v*M.cols] = (float) M.data [u+v*M.cols]/ (float) 255;
			}
		}
		
	}
	
	
	void convolution (const cv:: Mat& M, cv:: Mat& kernel, cv:: Mat& output_conv) {
		
		output_conv = cv::Mat (M.rows, M.cols, CV_32FC1);
		
		float* conv_float = reinterpret_cast <float *> (output_conv.data);	
		float* input_float = reinterpret_cast <float *> (M.data);
		float* kernel_float = reinterpret_cast <float *> (kernel.data);
		
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				
				conv_float[u+v*M.cols] = 0.0;
				
				for(int i =0;i<kernel.rows;++i) {
					for(int j=0;j<kernel.cols;++j) {
						if (v==0 || v== M.rows-1|| u==0 || u== M.cols-1) {
							conv_float[u+v*M.cols] = 0.0;
						} else {
							conv_float[u+v*M.cols] += (input_float[(u-1+j)+(v-1+i)*M.cols]) * (kernel_float[j+i*kernel.cols]);
								}
					}
				}
			}		
		}
	}
	
	void rescale (const cv:: Mat& input, cv:: Mat& output_scale) {
		
		output_scale = cv::Mat (input.rows, input.cols, CV_32FC1);
		float max=0;
		float min=0;
		
		float* scale_float = reinterpret_cast <float *> (output_scale.data);	
		float* input_float = reinterpret_cast <float *> (input.data);
			
		for(int v =0;v<input.rows;++v) {
			for(int u=0;u<input.cols;++u) {
				if (max < input_float[u+v*input.cols]) {
					max= input_float[u+v*input.cols];	
				}
				if (min > input_float[u+v*input.cols]) {
					min= input_float[u+v*input.cols];
				}
			}
		}
		
		for(int v =0;v<input.rows;++v) {
			for(int u=0;u<input.cols;++u) {
				scale_float[u+v*input.cols] = (input_float[u+v*input.cols] + fabs(min)) / (fabs(min)+max);
			}
		}
		
	}	
		
	void sobel (const cv:: Mat& input, cv:: Mat& magnitude,cv:: Mat& orientation) {
	
		cv::Mat vert_sobel;
		cv::Mat oriz_sobel;
		
		cv::Mat mat2;
		cv::Mat mat3;
		cv::Mat mat4;
		
				
		float data_vert_sobel [9] = { -1.0, 0.0, 1.0, -2.0, 0.0, 2.0, -1.0, 0.0, 1.0 };		
		vert_sobel = cv::Mat (3, 3, CV_32FC1, data_vert_sobel);
		
		float data_oriz_sobel [9] = { -1.0, -2.0, -1.0, 0.0, 0.0, 0.0, 1.0, 2.0, 1.0 };		
		oriz_sobel = cv::Mat (3, 3, CV_32FC1, data_oriz_sobel);
						
		grey_float (input, mat2);
		convolution(mat2, vert_sobel, mat3);	//sobel vertical
		convolution(mat2, oriz_sobel, mat4);	//sobel orizontal
		
		magnitude = cv::Mat (input.rows, input.cols, CV_32FC1);
		orientation = cv::Mat (input.rows, input.cols, CV_32FC1);
		
		float* sobel_vertical_float = reinterpret_cast <float *> (mat3.data);	
		float* sobel_orizontal_float = reinterpret_cast <float *> (mat4.data);
		float* magnitude_float = reinterpret_cast <float *> (magnitude.data);
		float* orientation_float = reinterpret_cast <float *> (orientation.data);
		
		cv::namedWindow("vertical_sobel", cv::WINDOW_NORMAL);
		cv::imshow("vertical_sobel", mat3);
		
		cv::namedWindow("oriz_sobel", cv::WINDOW_NORMAL);
		cv::imshow("oriz_sobel", mat4);	
		
		for(int v =0;v<input.rows;++v) {
			for(int u=0;u<input.cols;++u) {
				magnitude_float[u+v*input.cols]= sqrt((pow(sobel_vertical_float[u+v*input.cols],2.0) + pow(sobel_orizontal_float[u+v*input.cols],2.0)));
				orientation_float[u+v*input.cols] = atan2 (sobel_orizontal_float[u+v*input.cols],sobel_vertical_float[u+v*input.cols]) + M_PI;
			}
		}	
		
		cv::Mat magnitude_rescale;
		rescale (magnitude, magnitude_rescale);	
		cv::namedWindow("magnitudo", cv::WINDOW_NORMAL);
		cv::imshow("magnitudo", magnitude_rescale);
				
		cv::Mat adjMap;
		cv::convertScaleAbs(orientation, adjMap, 255 /(2* M_PI));
		cv::Mat falseColorsMap;
		cv::applyColorMap(adjMap, falseColorsMap,
		cv::COLORMAP_AUTUMN);
		cv::imshow("Out", falseColorsMap);
		
	}
	
		
int main(int argc, char **argv)
{
	int frame_number = 0;
	char frame_name[256];
	bool exit_loop = false;

	std::cout<<"Simple program."<<std::endl;

	//////////////////////
	//parse argument list:
	//////////////////////
	ArgumentList args;
	if(!ParseInputs(args, argc, argv)) {
		return 1;
	}

	while(!exit_loop)
	{
		//generating file name
		//
		//multi frame case
		if(args.image_name.find('%') != std::string::npos)
			sprintf(frame_name,(const char*)(args.image_name.c_str()),frame_number);
		else //single frame case
			sprintf(frame_name,"%s",args.image_name.c_str());

		//opening file
		std::cout<<"Opening "<<frame_name<<std::endl;

		cv::Mat image = cv::imread(frame_name, cv::IMREAD_GRAYSCALE);
		if(image.empty())
		{
			std::cout<<"Unable to open "<<frame_name<<std::endl;
			return 1;
		}


		//////////////////////
		//processing code here
		cv::Mat magnitudo;
		cv::Mat orientazione;
		
		sobel(image,magnitudo, orientazione);

		/////////////////////

		//display image
		
		//cv::namedWindow("grey_input", cv::WINDOW_NORMAL);
		//cv::imshow("grey_input", image);
		
			
		//wait for key or timeout
		unsigned char key = cv::waitKey(args.wait_t);
		std::cout<<"key "<<int(key)<<std::endl;

		//here you can implement some looping logic using key value:
		// - pause
		// - stop
		// - step back
		// - step forward
		// - loop on the same frame
		if(key == 'q')
			exit_loop = true;

		frame_number++;
	}

	return 0;
}
