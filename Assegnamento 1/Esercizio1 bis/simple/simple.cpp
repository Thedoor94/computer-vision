//OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//std:
#include <fstream>
#include <iostream>
#include <string>

struct ArgumentList {
	std::string image_name;		    //!< image file name
	int wait_t;                     //!< waiting time
};

bool ParseInputs(ArgumentList& args, int argc, char **argv) {

	if(argc<3 || (argc==2 && std::string(argv[1]) == "--help") || (argc==2 && std::string(argv[1]) == "-h") || (argc==2 && std::string(argv[1]) == "-help"))
	{
		std::cout<<"usage: simple -i <image_name>"<<std::endl;
		std::cout<<"exit:  type q"<<std::endl<<std::endl;
		std::cout<<"Allowed options:"<<std::endl<<
				"   -h	                     produce help message"<<std::endl<<
				"   -i arg                   image name. Use %0xd format for multiple images."<<std::endl<<
				"   -t arg                   wait before next frame (ms) [default = 0]"<<std::endl<<std::endl<<std::endl;
		return false;
	}

	int i = 1;
	while(i<argc)
	{
		if(std::string(argv[i]) == "-i") {
			args.image_name = std::string(argv[++i]);
		}

		if(std::string(argv[i]) == "-t") {
			args.wait_t = atoi(argv[++i]);
		}
		else
			args.wait_t = 0;

		++i;
	}

	return true;
}
	
	void grey (const cv:: Mat& M, cv:: Mat& output) {
						
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				
				float b=0;
				float g=0;
				float r=0;
				float y=0;
		
				b= M.data[ (u + v*M.cols)*3];	//B
				g= M.data[ (u + v*M.cols)*3 + 1];	//G
				r= M.data[ (u + v*M.cols)*3 + 2];	//R
				
				y= b*0.114 + g*0.587 + r*0.299;
				output.data[u + v*M.cols] =y;
			}
		}
	}
	
	void grey_float (const cv:: Mat& M, cv:: Mat& output_conv) {
		
		output_conv = cv::Mat (M.rows, M.cols, CV_32FC1);
		
		float* convert_float = reinterpret_cast <float *> (output_conv.data);	
						
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				convert_float[u+v*M.cols] = (float) M.data [u+v*M.cols]/ (float) 255;
			}
		}
		
	}
	
	void convolution (const cv:: Mat& M, cv:: Mat& kernel, cv:: Mat& output_conv) {
		
		int kernel_row =3;
		int kernel_cols =3;
		
		float data [9] = {0.1, 0.1, 0.1, 0.2,0.1, 0.1, 0.1, 0.1,0.1};		
		kernel = cv::Mat (kernel_row, kernel_cols, CV_32FC1, data);
		
		output_conv = cv::Mat (M.rows, M.cols, CV_32FC1);
		
		float* conv_float = reinterpret_cast <float *> (output_conv.data);	
		float* input_float = reinterpret_cast <float *> (M.data);
		float* kernel_float = reinterpret_cast <float *> (kernel.data);
		
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				
				conv_float[u+v*M.cols] = 0.0;
				
				for(int i =0;i<kernel.rows;++i) {
					for(int j=0;j<kernel.cols;++j) {
						if (v==0 || v== M.rows-1|| u==0 || u== M.cols-1) {
							conv_float[u+v*M.cols] = 0.0;
						} else {
							conv_float[u+v*M.cols] += (input_float[u+i+(v+j)*M.cols]) * (kernel_float[i+j*kernel.cols]);
								}
					}
				}
			}		
		}
	}
	
	void float_to_int (const cv:: Mat& M, cv:: Mat& output_conv_int) {
		output_conv_int = cv::Mat (M.rows, M.cols, CV_8UC1);
		float* input_float = reinterpret_cast <float *> (M.data);
		
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
			output_conv_int.data[u+v*M.cols] = (input_float [u+v*M.cols])*255;
			}
		}
	}
		
int main(int argc, char **argv)
{
	int frame_number = 0;
	char frame_name[256];
	bool exit_loop = false;

	std::cout<<"Simple program."<<std::endl;

	//////////////////////
	//parse argument list:
	//////////////////////
	ArgumentList args;
	if(!ParseInputs(args, argc, argv)) {
		return 1;
	}

	while(!exit_loop)
	{
		//generating file name
		//
		//multi frame case
		if(args.image_name.find('%') != std::string::npos)
			sprintf(frame_name,(const char*)(args.image_name.c_str()),frame_number);
		else //single frame case
			sprintf(frame_name,"%s",args.image_name.c_str());

		//opening file
		std::cout<<"Opening "<<frame_name<<std::endl;

		cv::Mat image = cv::imread(frame_name);
		if(image.empty())
		{
			std::cout<<"Unable to open "<<frame_name<<std::endl;
			return 1;
		}


		//////////////////////
		//processing code here
		cv::Mat mat2;
		cv::Mat mat3;
		cv::Mat mat4;
		
		cv::Mat kernel;
		cv::Mat output_grey = cv::Mat (image.rows, image.cols, CV_8UC1);
		
		grey(image,output_grey);
		grey_float (output_grey, mat2);
		convolution (mat2, kernel, mat3);
		float_to_int (mat3, mat4);

		/////////////////////

		//display image
		cv::namedWindow("grey", cv::WINDOW_NORMAL);
		cv::imshow("grey", output_grey);
		
		cv::namedWindow("grey_float", cv::WINDOW_NORMAL);
		cv::imshow("grey_float", mat2 );
		
		cv::namedWindow("convolution_float", cv::WINDOW_NORMAL);
		cv::imshow("convolution_float", mat3 );	
		
		cv::namedWindow("convolution_int", cv::WINDOW_NORMAL);
		cv::imshow("convolution_int", mat4 );	
			

		//wait for key or timeout
		unsigned char key = cv::waitKey(args.wait_t);
		std::cout<<"key "<<int(key)<<std::endl;

		//here you can implement some looping logic using key value:
		// - pause
		// - stop
		// - step back
		// - step forward
		// - loop on the same frame
		if(key == 'q')
			exit_loop = true;

		frame_number++;
	}

	return 0;
}
