//OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//std:
#include <fstream>
#include <iostream>
#include <string>

//math
#include <cmath>

struct ArgumentList {
	std::string image_name;		    //!< image file name
	int wait_t;                     //!< waiting time
};

bool ParseInputs(ArgumentList& args, int argc, char **argv) {

	if(argc<3 || (argc==2 && std::string(argv[1]) == "--help") || (argc==2 && std::string(argv[1]) == "-h") || (argc==2 && std::string(argv[1]) == "-help"))
	{
		std::cout<<"usage: simple -i <image_name>"<<std::endl;
		std::cout<<"exit:  type q"<<std::endl<<std::endl;
		std::cout<<"Allowed options:"<<std::endl<<
				"   -h	                     produce help message"<<std::endl<<
				"   -i arg                   image name. Use %0xd format for multiple images."<<std::endl<<
				"   -t arg                   wait before next frame (ms) [default = 0]"<<std::endl<<std::endl<<std::endl;
		return false;
	}

	int i = 1;
	while(i<argc)
	{
		if(std::string(argv[i]) == "-i") {
			args.image_name = std::string(argv[++i]);
		}

		if(std::string(argv[i]) == "-t") {
			args.wait_t = atoi(argv[++i]);
		}
		else
			args.wait_t = 0;

		++i;
	}

	return true;
}
	
	void grey (const cv:: Mat& M, cv:: Mat& output) {
						
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				
				float b=0;
				float g=0;
				float r=0;
				float y=0;
		
				b= M.data[ (u + v*M.cols)*3];	//B
				g= M.data[ (u + v*M.cols)*3 + 1];	//G
				r= M.data[ (u + v*M.cols)*3 + 2];	//R
				
				y= b*0.114 + g*0.587 + r*0.299;
				output.data[u + v*M.cols] =y;
			}
		}
	}
	
	void grey_float (const cv:: Mat& M, cv:: Mat& output_conv) {
		
		output_conv = cv::Mat (M.rows, M.cols, CV_32FC1);
		
		float* convert_float = reinterpret_cast <float *> (output_conv.data);	
						
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				convert_float[u+v*M.cols] = (float) M.data [u+v*M.cols]/ (float) 255;
			}
		}
		
	}
	
	
	void convolution (const cv:: Mat& M, cv:: Mat& kernel, cv:: Mat& output_conv) {
		
		output_conv = cv::Mat (M.rows, M.cols, CV_32FC1);
		
		float* conv_float = reinterpret_cast <float *> (output_conv.data);	
		float* input_float = reinterpret_cast <float *> (M.data);
		float* kernel_float = reinterpret_cast <float *> (kernel.data);
		
		for(int v =0;v<M.rows;++v) {
			for(int u=0;u<M.cols;++u) {
				
				conv_float[u+v*M.cols] = 0.0;
				
				for(int i =0;i<kernel.rows;++i) {
					for(int j=0;j<kernel.cols;++j) {
						if (v==0 || v== M.rows-1|| u==0 || u== M.cols-1) {
							conv_float[u+v*M.cols] = 0.0;
						} else {
							conv_float[u+v*M.cols] += (input_float[(u-1+j)+(v-1+i)*M.cols]) * (kernel_float[j+i*kernel.cols]);
								}
					}
				}
			}		
		}
	}
		
	void rescale (const cv:: Mat& input, cv:: Mat& output_scale) {
		
		output_scale = cv::Mat (input.rows, input.cols, CV_32FC1);
		float max=0;
		float min=0;
		
		float* scale_float = reinterpret_cast <float *> (output_scale.data);	
		float* input_float = reinterpret_cast <float *> (input.data);
			
		for(int v =0;v<input.rows;++v) {
			for(int u=0;u<input.cols;++u) {
				if (max < input_float[u+v*input.cols]) {
					max= input_float[u+v*input.cols];	
				}
				if (min > input_float[u+v*input.cols]) {
					min= input_float[u+v*input.cols];
				}
			}
		}
		
		for(int v =0;v<input.rows;++v) {
			for(int u=0;u<input.cols;++u) {
				scale_float[u+v*input.cols] = (input_float[u+v*input.cols] + fabs(min)) / (fabs(min)+max);
			}
		}
		
	}	
	
	void gaussian_oriz_kernel(float sigma, int radius,cv::Mat& kernel_oriz) {
		
		int kernel_row =1;
		int kernel_cols =(2*radius)+1;
		
		kernel_oriz = cv::Mat (kernel_row, kernel_cols, CV_32FC1);
		float* kernel_float = reinterpret_cast <float *> (kernel_oriz.data);	
		
		float sum=0.0;
		
		for(int v =0;v<kernel_oriz.rows;++v) {
			for(int u=0;u<kernel_oriz.cols;++u) {
				kernel_float[u+v*kernel_oriz.cols]= (1.0/(2.0*M_PI*pow(sigma,2.0)))* exp (-((pow(u-radius,2.0)))/(2.0*sigma*sigma));
				sum += kernel_float[u+v*kernel_oriz.cols];
			}
		}
		
		//normalization
		for(int v =0;v<kernel_oriz.rows;++v) {
			for(int u=0;u<kernel_oriz.cols;++u) {
				kernel_float[u+v*kernel_oriz.cols] = kernel_float[u+v*kernel_oriz.cols]/ sum;
			}
		}
	}
	
	void gaussian_vert_kernel (const cv::Mat& kernel_oriz, cv::Mat& kernel_vert) {
	
		kernel_vert = cv::Mat (kernel_oriz.cols, kernel_oriz.rows, CV_32FC1);
		
		float* kernel_float = reinterpret_cast <float *> (kernel_vert.data);	
		float* kernel_float_oriz = reinterpret_cast <float *> (kernel_oriz.data);	
				
		for(int v =0;v<kernel_vert.rows;++v) {
			for(int u=0;u<kernel_vert.cols;++u) {
				kernel_float[u+v*kernel_vert.cols]= kernel_float_oriz[v+u*kernel_vert.cols];
			}
		}
	}
		
int main(int argc, char **argv)
{
	int frame_number = 0;
	char frame_name[256];
	bool exit_loop = false;

	std::cout<<"Simple program."<<std::endl;

	//////////////////////
	//parse argument list:
	//////////////////////
	ArgumentList args;
	if(!ParseInputs(args, argc, argv)) {
		return 1;
	}

	while(!exit_loop)
	{
		//generating file name
		//
		//multi frame case
		if(args.image_name.find('%') != std::string::npos)
			sprintf(frame_name,(const char*)(args.image_name.c_str()),frame_number);
		else //single frame case
			sprintf(frame_name,"%s",args.image_name.c_str());

		//opening file
		std::cout<<"Opening "<<frame_name<<std::endl;

		cv::Mat image = cv::imread(frame_name);
		if(image.empty())
		{
			std::cout<<"Unable to open "<<frame_name<<std::endl;
			return 1;
		}


		//////////////////////
		//processing code here
		cv::Mat mat2;
		cv::Mat mat3;
		cv::Mat mat4;
		cv::Mat mat5;	
		cv::Mat mat6;
		cv::Mat mat7;	
		cv::Mat mat8;
		cv::Mat mat9;	
		cv::Mat mat10;
		cv::Mat mat11;
				
		cv::Mat output_grey = cv::Mat (image.rows, image.cols, CV_8UC1);
		
		cv::Mat kernel_deriv;
		cv::Mat kernel_lapl;
		cv::Mat kernel_oriz;
		cv::Mat kernel_vert;
		
		float sigma=1.0;
		int radius=1;
		
		float data_derivativo [3] = { -1.0, 0.0, 1.0 };		
		kernel_deriv = cv::Mat (1, 3, CV_32FC1, data_derivativo);
		
		float data_laplaciano [9] = { 0.0, 1.0, 0.0, 1.0, -4.0, 1.0, 0.0, 1.0, 0.0};		
		kernel_lapl = cv::Mat (3, 3, CV_32FC1, data_laplaciano);	
		
		gaussian_oriz_kernel(sigma, radius, kernel_oriz);
		gaussian_vert_kernel (kernel_oriz, kernel_vert);
		
		grey(image,output_grey);
		grey_float (output_grey, mat2);
		
		convolution (mat2, kernel_oriz, mat3);	//applico filtro gaussiano verticale
		convolution (mat3, kernel_deriv, mat6); //applico filtro derivativo
		rescale(mat6, mat7);	//rescale
		
		convolution (mat2, kernel_vert, mat4);	//applico filtro gaussiano orizzontale
		convolution (mat4, kernel_deriv, mat8);	//applico filtro derivativo
		rescale(mat8, mat9);	//rescale
		
		convolution (mat3, kernel_vert, mat5);	//applico filtro bidimensionale
		
		convolution (mat2, kernel_lapl, mat10);	//applico filtro gaussiano laplaciano
		rescale(mat10,mat11);
		

		/////////////////////

		//display image
		
		cv::namedWindow("grey_float", cv::WINDOW_NORMAL);
		cv::imshow("grey_float", mat2);
		
		cv::namedWindow("deriv_gaus_oriz", cv::WINDOW_NORMAL);
		cv::imshow("deriv_gaus_oriz", mat7 );		
		
		cv::namedWindow("deriv_gaus_vert", cv::WINDOW_NORMAL);
		cv::imshow("deriv_gaus_vert", mat9 );	
		
		cv::namedWindow("laplaciano", cv::WINDOW_NORMAL);
		cv::imshow("laplaciano", mat11);
			
		//wait for key or timeout
		unsigned char key = cv::waitKey(args.wait_t);
		std::cout<<"key "<<int(key)<<std::endl;

		//here you can implement some looping logic using key value:
		// - pause
		// - stop
		// - step back
		// - step forward
		// - loop on the same frame
		if(key == 'q')
			exit_loop = true;

		frame_number++;
	}

	return 0;
}
