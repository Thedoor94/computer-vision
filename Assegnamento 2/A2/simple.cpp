//OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"

//std:
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <iterator>
#include <math.h>

using namespace cv;

struct ArgumentList {
	std::string image_name;		    //!< image file name
};


bool ParseInputs(ArgumentList& args, int argc, char **argv) {

	if(argc<3 || (argc==2 && std::string(argv[1]) == "--help") || (argc==2 && std::string(argv[1]) == "-h") || (argc==2 && std::string(argv[1]) == "-help"))
	{
		std::cout<<"usage: simple -i <image_name>"<<std::endl;
		std::cout<<"exit:  type q"<<std::endl<<std::endl;
		std::cout<<"Allowed options:"<<std::endl<<
				"   -h	                     produce help message"<<std::endl<<
				"   -i arg                   image name. Use %0xd format for multiple images."<<std::endl;
		return false;
	}

	int i = 1;
	while(i<argc)
	{
		if(std::string(argv[i]) == "-i") {
			args.image_name = std::string(argv[++i]);
		}
		++i;
	}

	return true;
}

void imageIntoFloat(const cv::Mat& input, cv::Mat& output){

	output = cv::Mat (input.rows, input.cols, CV_32FC1);
	float* output_float = reinterpret_cast<float * >(output.data);
	for(int y=0; y< input.rows; y++){
		for(int x=0; x< input.cols; x++){
			output_float[(x+y*input.cols)] = float(input.data[(x+y*input.cols)])/float(255);
		}
	}
}

void rescale (const cv:: Mat& input, cv:: Mat& output_scale) {
	
	output_scale = cv::Mat (input.rows, input.cols, CV_32FC1);
	
	float max=0;
	float min=0;
	
	float* scale_float = reinterpret_cast <float *> (output_scale.data);	
	float* input_float = reinterpret_cast <float *> (input.data);
		
	for(int v =0;v<input.rows;++v) {
		for(int u=0;u<input.cols;++u) {
			if (max < input_float[u+v*input.cols]) {
				max= input_float[u+v*input.cols];	
			}
			if (min > input_float[u+v*input.cols]) {
				min= input_float[u+v*input.cols];
			}
		}
	}
	
	for(int v =0;v<input.rows;++v) {
		for(int u=0;u<input.cols;++u) {
			scale_float[u+v*input.cols] = (input_float[u+v*input.cols] + fabs(min)) / (fabs(min)+fabs(max));
		}
	}
	
}	

template <class T>
T bilinearAt(const cv::Mat& image, float r, float c)
{
    float s = c - std::floor(c);
    float t = r - std::floor(r);

    int floor_r = (int) std::floor(r);
    int floor_c = (int) std::floor(c);

    int ceil_r = (int) std::ceil(r);
    int ceil_c = (int) std::ceil(c);

    T value = (1 - s) * (1 - t) * image.at<T>(floor_r, floor_c)
                + s * (1 - t) * image.at<T>(floor_r, ceil_c)
                + (1 - s) * t * image.at<T>(ceil_r, floor_c)
                + s * t * image.at<T>(ceil_r, ceil_c);

    return value;
}

void non_maximu_suppression(cv:: Mat image, std::vector<cv::KeyPoint> & keypoints0, float harrisTh) {

	for (int y=0; y< image.cols; y++) {
		for (int x=0; x< image.rows; x++) {
			if (image.at<float> (x,y) <= harrisTh) {
				image.at<float> (x,y)=0;
			} 
		}
	}
	
	for (int y=1; y< image.cols-1; y++) {
		for (int x=1; x< image.rows-1; x++) {
			if (image.at<float> (x,y) <= image.at<float> (x-1,y-1) || image.at<float> (x,y) <= image.at<float> (x,y-1)
					|| image.at<float> (x,y) <= image.at<float> (x+1,y-1) || image.at<float> (x,y) <= image.at<float> (x-1,y)
						|| image.at<float> (x,y) <= image.at<float> (x+1,y) || image.at<float> (x,y) <= image.at<float> (x-1,y+1)
							|| image.at<float> (x,y) <= image.at<float> (x,y+1) || image.at<float> (x,y) <= image.at<float> (x+1,y+1) ) {
				image.at<float> (x,y)=0;
			}
		}
	}
	
	for (int y=0; y< image.cols; y++) {
		for (int x=0; x< image.rows; x++) {
			if (image.at<float> (x,y) > 0) {
				keypoints0.push_back(cv::KeyPoint(float(y), float(x), 3));
			}
		}
	}
}
	
void harrisCornerDetector(const cv::Mat image, std::vector<cv::KeyPoint> & keypoints0, float alpha, float harrisTh)
{
	Mat dst1,dst2, product, square_vert, square_oriz,kernel_oriz, kernel_vert, gaussian,square_oriz_gaus, square_vert_gaus, product_gaus, formula, formula_res;
	Point anchor;
	
	double delta;
	int ddepth;
	int kernel_size;
	
	anchor = Point( -1, -1 );
	delta = 0;
	ddepth = -1;
	
	kernel_oriz= (cv::Mat_<float>(3, 1)<< -1.0, 0.0, 1.0);
	kernel_vert= (cv::Mat_<float>(1, 3)<< -1.0, 0.0, 1.0);
	
	filter2D(image, dst1, ddepth , kernel_oriz, anchor, delta, BORDER_DEFAULT );
	filter2D(image, dst2, ddepth , kernel_vert, anchor, delta, BORDER_DEFAULT );	
	
	square_oriz= dst1.mul(dst1);
	square_vert = dst2.mul(dst2);
	product= dst1.mul(dst2);
	
	GaussianBlur( square_oriz, square_oriz_gaus, Size( 5, 5 ), 0, 0 );
	GaussianBlur( square_vert, square_vert_gaus, Size( 5, 5 ), 0, 0 );
	GaussianBlur( product, product_gaus, Size( 5, 5 ), 0, 0 );
	
	formula = square_oriz_gaus.mul(square_vert_gaus) - (product_gaus.mul(product_gaus)) - alpha * ((square_oriz_gaus + square_vert_gaus).mul(square_oriz_gaus + square_vert_gaus));
	
	rescale (formula, formula_res);	//porto tutto nel range (0:1)
	
	non_maximu_suppression(formula_res, keypoints0, harrisTh);
		
	cv::Mat adjMap;
	cv::Mat falseColorsMap;
	double minr,maxr;
	cv::minMaxLoc(formula_res, &minr, &maxr);
	cv::convertScaleAbs(formula_res, adjMap, 255 / (maxr-minr));
	cv::applyColorMap(adjMap, falseColorsMap, cv::COLORMAP_RAINBOW);
	cv::namedWindow("response1", cv::WINDOW_NORMAL);
	cv::imshow("response1", falseColorsMap);
	
}

void findHomographyRansac(const std::vector<cv::Point2f> & points1, const std::vector<cv::Point2f> & points0, int N, float epsilon, int sample_size, cv::Mat & H, std::vector<cv::Point2f> & inliers_best0, std::vector<cv::Point2f> & inliers_best1)
{	 
	 srand(time(NULL));
	 
	 cv:: Point2f p1;
	 cv:: Point2f p0;
	 
	 std::vector<cv::Point2f> points1_ransac;
	 std::vector<cv::Point2f> points0_ransac;
	 	
	 std::vector<cv::Point2d> inliers_best0_test;
	 std::vector<cv::Point2d> inliers_best1_test;	
	 
	 cv::Mat H_test; 
	 double res=0;
	 int max=0, cont=0, random=0;
	
	 for (int j=0; j< N; j++) {
		 
		for (int i=0; i< 4; i++) {	
		 
			random= rand() % points0.size();	
					
			p1 =  points1[random];
			p0 =  points0[random];					
			
			points1_ransac.push_back(p1);
			points0_ransac.push_back(p0);
		}
				
		H_test= cv::findHomography(cv::Mat (points1_ransac), cv::Mat (points0_ransac), 0);	 	//Calcolo omografia con i 4 keypoints associati e presi casualmente		
		
		for (unsigned int u=0; u< points1.size();u++) {
			
			cv::Mat p1_omog = (cv::Mat_<double>(3, 1) << points1[u].x,points1[u].y, 1.0 );	
			cv::Mat product = (cv::Mat_<double>(3, 1));	

			product = H_test*p1_omog;			
			
			cv::Point2d product_point(product.at<double> (0,0)/product.at<double>(2,0), product.at<double> (1,0)/product.at<double>(2,0));	
												
			res= sqrt(pow(points0[u].x - product_point.x,2)+ pow(points0[u].y - product_point.y,2));	//calcolo della distanza euclidea
					
			if (res < epsilon) {
				inliers_best1_test.push_back(points1[u]);
				inliers_best0_test.push_back(points0[u]);
				cont++;			
			}		
			
		}
					
		if (cont > max) {

			max = cont;
			inliers_best0.clear();
			inliers_best1.clear();
			
			for (unsigned int i=0; i< inliers_best0_test.size(); i++) {
				
				inliers_best0.push_back(inliers_best0_test[i]);
				inliers_best1.push_back(inliers_best1_test[i]);						
			}		
		}
		
		cont=0;
		
		inliers_best0_test.clear();		
		inliers_best1_test.clear();	
							
		points1_ransac.clear();
		points0_ransac.clear();				
	}
	
	H= cv::findHomography(cv::Mat (inliers_best1), cv::Mat (inliers_best0), 0);	//calcolo omografia con il gruppo di inliers più numeroso trovato
}

int main(int argc, char **argv)
{
	int frame_number = 0;
	char frame_name[256];
	bool exit_loop = false;

	//vettore delle immagini di input
	std::vector<cv::Mat> imageRGB_v;
	//vettore delle immagini di input grey scale
	std::vector<cv::Mat> image_v;

	std::cout<<"Simple image stitching program."<<std::endl;

	//////////////////////
	//parse argument list:
	//////////////////////
	ArgumentList args;
	if(!ParseInputs(args, argc, argv)) {
		return 1;
	}

	while(!exit_loop)
	{
		//generating file name
		//
		//multi frame case
		if(args.image_name.find('%') != std::string::npos)
			sprintf(frame_name,(const char*)(args.image_name.c_str()),frame_number);

		cv::Mat im = cv::imread(frame_name);
		if(im.empty())
		{
			break;
		}

		//opening file
		std::cout<<"Opening "<<frame_name<<std::endl;

		//save RGB image
		imageRGB_v.push_back(im);

		//save grey scale image for processing
		cv::Mat im_grey(im.rows, im.cols, CV_8UC1);
		cvtColor(im, im_grey, CV_RGB2GRAY);
		image_v.push_back(im_grey);

		frame_number++;
	}

	if(image_v.size()<2)
	{
		std::cout<<"At least 2 images are required. Exiting."<<std::endl;
		return 1;
	}

	int image_width = image_v[0].cols;
	int image_height = image_v[0].rows;

	////////////////////////////////////////////////////////
	/// HARRIS CORNER
	//
	float alpha = 0.04;
	float harrisTh=0.22;     //impostato in modo tale da avere un buon numero di punti

	std::vector<cv::KeyPoint> keypoints0, keypoints1;
	cv:Mat temp1, temp2;	
	
	imageIntoFloat(image_v[0], temp1);
	imageIntoFloat(image_v[1], temp2);
	
	harrisCornerDetector(temp1, keypoints0, alpha, harrisTh);
	harrisCornerDetector(temp2, keypoints1, alpha, harrisTh);
	////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////
    /// CALCOLO DESCRITTORI E MATCHES
	//
    int briThreshl=30;
    int briOctaves = 3;
    int briPatternScales = 1.0;
	cv::Mat descriptors0, descriptors1;

	//dichiariamo un estrattore di features di tipo BRISK
    cv::Ptr<cv::DescriptorExtractor> extractor = cv::BRISK::create(briThreshl, briOctaves, briPatternScales);
    //calcoliamo il descrittore di ogni keypoint
    extractor->compute(image_v[0], keypoints0, descriptors0);
    extractor->compute(image_v[1], keypoints1, descriptors1);

    //associamo i descrittori tra me due immagini
    std::vector<std::vector<cv::DMatch> > matches;
	cv::BFMatcher matcher = cv::BFMatcher(cv::NORM_HAMMING);
	matcher.radiusMatch(descriptors0, descriptors1, matches, image_v[0].cols*0.2);

    std::vector<cv::Point2f> points[2];
    for(unsigned int i=0; i<matches.size(); ++i)
      {
        if(!matches.at(i).empty())
          {
                points[0].push_back(keypoints0.at(matches.at(i).at(0).queryIdx).pt);
                points[1].push_back(keypoints1.at(matches.at(i).at(0).trainIdx).pt);
          }
      }
	////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////
    // CALCOLO OMOGRAFIA
    //
    //
    // E' obbligatorio implementare RANSAC.
    //
    // Per testare i corner di Harris inizialmente potete utilizzare findHomography di opencv, che include gia' RANSAC
    //
    // Una volta che avete verificato che i corner funzionano, passate alla vostra implementazione di RANSAC
    //
    //
    cv::Mat H;            //omografia finale
	std::vector<cv::Point2f> inliers_best[2]; //inliers
    if(points[1].size()>=4)
    {
    	int N=100;            //numero di iterazioni di RANSAC
    	float epsilon = 0.5;  //distanza per il calcolo degli inliers
    	int sample_size = 4;  //dimensione del sample

    	//
    	//
    	// Abilitate questa funzione una volta che quella di opencv funziona
    	//
    	//
    	findHomographyRansac(points[1], points[0], N, epsilon, sample_size, H, inliers_best[0], inliers_best[1]);
    	//
    	//
    	//
    	//
    	//

    	std::cout<<std::endl<<"Risultati Ransac: "<<std::endl;
    	std::cout<<"Num inliers / match totali "<<inliers_best[0].size()<<" / "<<points[0].size()<<std::endl;

    	//
    	//
    	// Rimuovere questa chiamata solo dopo aver verificato che i vostri corner di Harris generano una omografia corretta
    	//
    	//
    	//H = cv::findHomography( cv::Mat(points[1]), cv::Mat(points[0]), CV_RANSAC );
    	//
    	//
    	//
    	//
    	//
    }
    else
    {
    	std::cout<<"Non abbastanza matches per calcolare H!"<<std::endl;
    	H = (cv::Mat_<double>(3, 3 )<< 1.0, 0.0, 0.0,
    		                           0.0, 1.0, 0.0,
			                           0.0, 0.0, 1.0);
    }

    std::cout<<"H"<<std::endl<<H<<std::endl;
    cv::Mat H_inv = H.inv();///H.at<double>(2,2);
    std::cout<<"H_inverse "<<std::endl<<H_inv<<std::endl;
	////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////
    /// CALCOLO DELLA DIMENSIONE DELL'IMMAGINE FINALE
    //
    cv::Mat p = (cv::Mat_<double>(3, 1) << 0, 0, 1);
    cv::Mat tl = H*p;
    tl/=tl.at<double>(2,0);
    p = (cv::Mat_<double>(3, 1) << image_width-1, image_height-1, 1);
    cv::Mat br = H*p;
    br/=br.at<double>(2,0);
    p = (cv::Mat_<double>(3, 1) << 0, image_height-1, 1);
    cv::Mat bl = H*p;
    bl/=bl.at<double>(2,0);
    p = (cv::Mat_<double>(3, 1) << image_width-1, 0, 1);
    cv::Mat tr = H*p;
    tr/=tr.at<double>(2,0);

    int min_warped_r = std::min(std::min(tl.at<double>(1,0), bl.at<double>(1,0)),std::min(tr.at<double>(1,0), br.at<double>(1,0)));
    int min_warped_c = std::min(std::min(tl.at<double>(0,0), bl.at<double>(0,0)),std::min(tr.at<double>(0,0), br.at<double>(0,0)));

    int max_warped_r = std::max(std::max(tl.at<double>(1,0), bl.at<double>(1,0)),std::max(tr.at<double>(1,0), br.at<double>(1,0)));
    int max_warped_c = std::max(std::max(tl.at<double>(0,0), bl.at<double>(0,0)),std::max(tr.at<double>(0,0), br.at<double>(0,0)));

    int min_final_r = std::min(min_warped_r,0);
    int min_final_c = std::min(min_warped_c,0);

    int max_final_r = std::max(max_warped_r,image_height-1);
    int max_final_c = std::max(max_warped_c,image_width-1);

    int width_final = max_final_c-min_final_c+1;
    int height_final = max_final_r-min_final_r+1;

    std::cout<<"width_final "<<width_final<<" height_final "<<height_final<<std::endl;
	////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////
    /// CALCOLO IMMAGINE FINALE
    //
    cv::Mat outwarp(height_final, width_final, CV_8UC3, cv::Scalar(0,0,0));

    //copio l'immagine 0 sul nuovo piano immagine, e' solo uno shift
    imageRGB_v[0].copyTo(outwarp(cv::Rect(std::max(0,-min_warped_c), std::max(0,-min_warped_r), image_width, image_height)));

    //copio l'immagine 1 nel piano finale
    //in questo caso uso la trasformazione prospettica
    for(int r=0;r<height_final;++r)
    {
        for(int c=0;c<width_final;++c)
        {
        	cv::Mat p = (cv::Mat_<double>(3, 1) << c+std::min(0,min_warped_c), r+std::min(0,min_warped_r), 1);
        	cv::Mat pi = H_inv*p;
        	pi/=pi.at<double>(2,0);

        	if(int(pi.at<double>(1,0))>=1 && int(pi.at<double>(1,0))<image_height-1 && int(pi.at<double>(0,0))>=1 && int(pi.at<double>(0,0))<image_width-1)
        	{
        		cv::Vec3b pick = bilinearAt<cv::Vec3b>(imageRGB_v[1], pi.at<double>(1,0), pi.at<double>(0,0));

        		//media
        		if(outwarp.at<cv::Vec3b>(r,c) != cv::Vec3b(0.0))
        			outwarp.at<cv::Vec3b>(r,c) =  (outwarp.at<cv::Vec3b>(r,c)*0.5 + pick*0.5);
        		else
        			outwarp.at<cv::Vec3b>(r,c) = pick;
        	}
        }
    }
	////////////////////////////////////////////////////////

	////////////////////////////
	//WINDOWS
	//
    for(unsigned int i = 0;i<keypoints0.size();++i)
    	cv::circle(imageRGB_v[0], cv::Point(keypoints0[i].pt.x , keypoints0[i].pt.y ), 5,  cv::Scalar(0), 2, 8, 0 );

    for(unsigned int i = 0;i<keypoints1.size();++i)
    	cv::circle(imageRGB_v[1], cv::Point(keypoints1[i].pt.x , keypoints1[i].pt.y ), 5,  cv::Scalar(0), 2, 8, 0 );

	cv::namedWindow("KeyPoints0", cv::WINDOW_AUTOSIZE);
	cv::imshow("KeyPoints0", imageRGB_v[0]);

	cv::namedWindow("KeyPoints1", cv::WINDOW_AUTOSIZE);
	cv::imshow("KeyPoints1", imageRGB_v[1]);

    cv::Mat matchsOutput(image_height, image_width*2, CV_8UC3);
    imageRGB_v[0].copyTo(matchsOutput(cv::Rect(0, 0, image_width, image_height)));
    imageRGB_v[1].copyTo(matchsOutput(cv::Rect(image_width, 0, image_width, image_height)));
    for(unsigned int i=0; i<points[0].size(); ++i)
    {
    	cv::Point2f p2shift = points[1][i];
    	p2shift.x+=imageRGB_v[0].cols;
    	cv::circle(matchsOutput, points[0][i], 3, cv::Scalar(0,0,255));
    	cv::circle(matchsOutput, p2shift, 3, cv::Scalar(0,0,255));
    	cv::line(matchsOutput, points[0][i], p2shift, cv::Scalar(255,0,0));
    }
	cv::namedWindow("Matches", cv::WINDOW_NORMAL);
	cv::imshow("Matches", matchsOutput);

    cv::Mat matchsOutputIn(image_height, image_width*2, CV_8UC3);
    imageRGB_v[0].copyTo(matchsOutputIn(cv::Rect(0, 0, image_width, image_height)));
    imageRGB_v[1].copyTo(matchsOutputIn(cv::Rect(image_width, 0, image_width, image_height)));
    for(unsigned int i=0; i<inliers_best[0].size(); ++i)
    {
    	cv::Point2f p2shift = inliers_best[1][i];
    	p2shift.x+=image_width;
    	cv::circle(matchsOutputIn, inliers_best[0][i], 3, cv::Scalar(0,0,255));
    	cv::circle(matchsOutputIn, p2shift, 3, cv::Scalar(0,0,255));
    	cv::line(matchsOutputIn, inliers_best[0][i], p2shift, cv::Scalar(255,0,0));
    }
	cv::namedWindow("Matches Inliers", cv::WINDOW_NORMAL);
	cv::imshow("Matches Inliers", matchsOutputIn);

	cv::namedWindow("Outwarp", cv::WINDOW_AUTOSIZE);
	cv::imshow("Outwarp", outwarp);

	cv::waitKey(0);
	////////////////////////////

	return 0;
}
