# Assegnamenti Visione Artificiale

Esercizi base sull'elaborazione delle immagini in C++.
Viene utilizzata la libreria OpenCV solamente per le operazioni basilari quali lettura e apertura dell'immagine e vettore cv::Mat.
Tutte le funzionalità svolte del primo assegnamento sono state sviluppate senza l'uso delle funzioni standard fornite da OpenCV a scopo didattico.

## Primo Assegnamento

Contenuto:

* Passaggio a toni di grigio
* Convoluzione fra matrici
* Blur gaussiano verticale, orizzontale e bidimensionale
* Filtro derivativo orizzontale e verticale
* Filtro Laplaciano
* Sobel (Magnitudo e orientazione)
* Bilinear interpolation
* Find Peaks
* Sogliatura con isteresi
* Canny Edge Detector

## Secondo Assegnamento

Contenuto:

* Image Stitching
* Corner di Harris
* Descrittori immagini
* FindHomography
* RANSAC
* Blend delle immagini


### Come runnare i file

I file sono tutti in formato .cpp ed è già presente l'eseguibile in ogni cartella. 
Necessita della libreria OpenCV per il corretto funzionamento.
Da terminale digitare ad esempio **./simple -i images/Lenna.jpg**.
In caso si voglia apportare modifiche al file sorgente è necessario ricompilare il file con il comando **make**.

